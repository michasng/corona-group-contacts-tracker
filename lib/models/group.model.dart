import 'package:Group_Presence/models/model.dart';
import 'package:flutter/foundation.dart';

class GroupModel extends Model {
  String name;
  List<int> members;

  GroupModel({
    @required int id,
    @required this.name,
    @required this.members,
  }) : super(id: id);

  @override
  GroupModel clone() {
    return GroupModel(
      id: id,
      name: name,
      members: List.from(members),
    );
  }

  @override
  String toString() {
    return '{ id: $id, name: $name, members: $members }';
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['members'] = members;
    return map;
  }

  static GroupModel fromMap(Map<String, dynamic> map) => GroupModel(
        id: map['id'],
        name: map['name'],
        members:
            (map['members'] as List).map<int>((value) => value as int).toList(),
      );
}
