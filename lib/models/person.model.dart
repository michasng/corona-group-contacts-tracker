import 'package:Group_Presence/models/model.dart';
import 'package:Group_Presence/services/time.service.dart';
import 'package:flutter/foundation.dart';

class PersonModel extends Model {
  String firstName;
  String lastName;
  String phoneNumber;
  String emailAddress;
  int registerTimestamp;

  PersonModel({
    @required int id,
    @required this.firstName,
    @required this.lastName,
    @required this.phoneNumber,
    @required this.emailAddress,
    @required this.registerTimestamp,
  }) : super(id: id);

  @override
  PersonModel clone() {
    return PersonModel(
      id: id,
      firstName: firstName,
      lastName: lastName,
      phoneNumber: phoneNumber,
      emailAddress: emailAddress,
      registerTimestamp: registerTimestamp,
    );
  }

  @override
  String toString() {
    return '{ id: $id, firstName: $firstName, lastName: $lastName, phoneNumber: $phoneNumber, emailAddress: $emailAddress, registerAt: ${TimeService.timestampToDateTimeString(registerTimestamp)} }';
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['firstName'] = firstName;
    map['lastName'] = lastName;
    map['phoneNumber'] = phoneNumber;
    map['emailAddress'] = emailAddress;
    map['registerTimestamp'] = registerTimestamp;
    return map;
  }

  static PersonModel fromMap(Map<String, dynamic> map) => PersonModel(
        id: map['id'],
        firstName: map['firstName'],
        lastName: map['lastName'],
        phoneNumber: map['phoneNumber'],
        emailAddress: map['emailAddress'],
        registerTimestamp: map['registerTimestamp'],
      );
}
