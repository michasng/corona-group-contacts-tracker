import 'package:flutter/foundation.dart';

abstract class Model {
  int id;

  Model({@required this.id});

  Model clone();

  Map<String, dynamic> toMap();
}
