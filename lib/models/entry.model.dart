import 'package:Group_Presence/models/model.dart';
import 'package:Group_Presence/services/time.service.dart';
import 'package:flutter/foundation.dart';

class EntryModel extends Model {
  List<int> persons;
  int fromTimestamp;
  int toTimestamp;

  EntryModel({
    @required int id,
    @required this.persons,
    @required this.fromTimestamp,
    @required this.toTimestamp,
  }) : super(id: id);

  @override
  EntryModel clone() {
    return EntryModel(
      id: id,
      persons: List.from(persons),
      fromTimestamp: fromTimestamp,
      toTimestamp: toTimestamp,
    );
  }

  @override
  String toString() {
    return '{ id: $id, persons: $persons, from: ${TimeService.timestampToDateTimeString(fromTimestamp)}, toTimestamp: ${TimeService.timestampToDateTimeString(toTimestamp)} }';
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['persons'] = persons;
    map['fromTimestamp'] = fromTimestamp;
    map['toTimestamp'] = toTimestamp;
    return map;
  }

  static EntryModel fromMap(Map<String, dynamic> map) => EntryModel(
        id: map['id'],
        persons:
            (map['persons'] as List).map<int>((value) => value as int).toList(),
        fromTimestamp: map['fromTimestamp'],
        toTimestamp: map['toTimestamp'],
      );
}
