import 'package:Group_Presence/services/entry.service.dart';
import 'package:Group_Presence/services/group.service.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.context.dart';
import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/widgets/pages/entry-page.dart';
import 'package:Group_Presence/widgets/pages/group-page.dart';
import 'package:Group_Presence/widgets/pages/home-page.dart';
import 'package:Group_Presence/widgets/pages/person-page.dart';
import 'package:Group_Presence/widgets/pages/settings-page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class IdArguments {
  final int id;

  IdArguments(this.id);

  factory IdArguments.fromContext(BuildContext context) {
    final dynamic args = ModalRoute.of(context).settings.arguments;
    if (args == null) return IdArguments(null);
    return args as IdArguments;
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final localStorageContext = LocalStorageModelContext();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PersonService>(
          create: (context) => PersonService(localStorageContext),
        ),
        ChangeNotifierProvider<GroupService>(
          create: (context) => GroupService(localStorageContext),
        ),
        ChangeNotifierProvider<EntryService>(
          create: (context) => EntryService(localStorageContext),
        ),
      ],
      builder: (context, child) => MaterialApp(
        initialRoute: HomePage.route,
        routes: {
          HomePage.route: (context) => HomePage(),
          PersonPage.route: (context) {
            final args = IdArguments.fromContext(context);
            return PersonPage(id: args.id);
          },
          GroupPage.route: (context) {
            final args = IdArguments.fromContext(context);
            return GroupPage(id: args.id);
          },
          EntryPage.route: (context) {
            final args = IdArguments.fromContext(context);
            return EntryPage(id: args.id);
          },
          SettingsPage.route: (context) => SettingsPage(),
        },
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
          accentColor: Colors.deepOrangeAccent,
        ),
        /*
          theme: ThemeData(
            brightness: Brightness.dark,
            primaryColor: Colors.black,
            accentColor: Colors.amberAccent,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          */
        // home: HomePage(),
      ),
    );
  }
}
