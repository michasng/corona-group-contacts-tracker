import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.context.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.service.dart';

class PersonService extends LocalStorageModelService<PersonModel> {
  PersonService(LocalStorageModelContext context)
      : super(context, 'persons', PersonModel.fromMap);
}
