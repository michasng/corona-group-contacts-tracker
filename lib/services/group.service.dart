import 'package:Group_Presence/models/group.model.dart';
import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.context.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.service.dart';

class GroupService extends LocalStorageModelService<GroupModel> {
  GroupService(LocalStorageModelContext context)
      : super(context, 'groups', GroupModel.fromMap);

  @override
  void onContextChanged() {
    final personService = context.serviceOf<PersonModel>();
    if (personService == null) return;
    personService.addOnRemove((id) {
      getAll().forEach((group) {
        if (group.members.contains(id)) {
          group.members.remove(id);
          modify(group);
        }
      });
    });
  }
}
