class TimeService {
  static int compare(int timestamp1, int timestamp2) {
    return DateTime.fromMillisecondsSinceEpoch(timestamp1)
        .compareTo(DateTime.fromMillisecondsSinceEpoch(timestamp2));
  }

  static String timestampToDateTimeString(int timestamp) {
    return toDateTimeString(DateTime.fromMillisecondsSinceEpoch(timestamp));
  }

  static String timestampToDateString(int timestamp) {
    return toDateString(DateTime.fromMillisecondsSinceEpoch(timestamp));
  }

  static String timestampToTimeString(int timestamp) {
    return toTimeString(DateTime.fromMillisecondsSinceEpoch(timestamp));
  }

  static String toDateTimeString(DateTime dateTime) {
    return '${toDateString(dateTime)}, ${toTimeString(dateTime)}';
  }

  static const _WEEKDAYS = ['Mo.', 'Di.', 'Mi.', 'Do.', 'Fr.', 'Sa.', 'So.'];
  static const _MONTHS = [
    'Jan.',
    'Feb.',
    'März',
    'Apr.',
    'Mai',
    'Juni',
    'Juli',
    'Aug.',
    'Sept.',
    'Okt.',
    'Nov.',
    'Dez.',
  ];
  static String toDateString(DateTime dateTime) {
    return '${_WEEKDAYS[dateTime.weekday - 1]}, ${dateTime.day.toString().padLeft(2, '0')}. ${_MONTHS[dateTime.month - 1]} ${dateTime.year}';
  }

  static String toTimeString(DateTime dateTime) {
    return '${dateTime.hour.toString().padLeft(2, '0')}:${dateTime.minute.toString().padLeft(2, '0')}';
  }
}
