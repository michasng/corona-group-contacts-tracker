import 'package:Group_Presence/models/model.dart';
import 'package:flutter/cupertino.dart';

abstract class ModelService<T extends Model> extends ChangeNotifier {
  List<T> getAll();
  T getOne(int id);
  bool hasAny();
  bool has(int id);
  int getNewId();
  void add(T model);
  void remove(int id);
  void modify(T model);
}
