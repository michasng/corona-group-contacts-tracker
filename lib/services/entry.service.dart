import 'package:Group_Presence/models/entry.model.dart';
import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.context.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.service.dart';

class EntryService extends LocalStorageModelService<EntryModel> {
  EntryService(LocalStorageModelContext context)
      : super(context, 'entries', EntryModel.fromMap);

  @override
  void onContextChanged() {
    final personService = context.serviceOf<PersonModel>();
    if (personService == null) return;
    personService.addOnRemove((id) {
      final List<EntryModel> toRemove = [];
      getAll().forEach((entry) {
        if (entry.persons.contains(id)) {
          entry.persons.remove(id);
          modify(entry);
        }
        if (entry.persons.isEmpty) toRemove.add(entry);
      });
      toRemove.forEach((entry) => remove(entry.id));
    });
  }
}
