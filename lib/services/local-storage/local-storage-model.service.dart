import 'dart:convert';

import 'package:Group_Presence/models/model.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.context.dart';
import 'package:Group_Presence/services/model.service.dart';
import 'package:universal_html/html.dart' as html;

typedef ModelRemoved = Function(int id);

class LocalStorageModelService<T extends Model> extends ModelService<T> {
  LocalStorageModelContext context;
  String _tableName;
  T Function(Map<String, dynamic>) _fromMap;

  List<ModelRemoved> _onRemove;

  List<T> _models = [];
  int _maxId = -1;

  LocalStorageModelService(this.context, this._tableName, this._fromMap) {
    _onRemove = [];
    context.registerService(this);
    _load();
  }

  void onContextChanged() {}

  void addOnRemove(ModelRemoved onRemove) {
    _onRemove.add(onRemove);
  }

  void _load() {
    var jsonStr = html.window.localStorage[_tableName];
    if (jsonStr == null) return;

    final modelsMap = jsonDecode(jsonStr);
    _models =
        (modelsMap['values'] as List).map<T>((map) => _fromMap(map)).toList();
    _findMaxId();
    print('load $_models');
    notifyListeners();
  }

  void _save() {
    var modelsMap = Map<String, dynamic>();
    modelsMap['values'] =
        _models.map<Map<String, dynamic>>((model) => model.toMap()).toList();

    final jsonStr = jsonEncode(modelsMap);
    html.window.localStorage[_tableName] = jsonStr;
    print('save $_models');
    notifyListeners();
  }

  void _findMaxId() {
    _models.forEach((model) {
      if (model.id > _maxId) _maxId = model.id;
    });
  }

  @override
  List<T> getAll() {
    return _models;
  }

  @override
  T getOne(int id) {
    return _models.firstWhere(
      (model) => model.id == id,
      orElse: () => null,
    );
  }

  @override
  bool hasAny() {
    return _models.length > 0;
  }

  @override
  bool has(int id) {
    return getOne(id) != null;
  }

  int getNewId() {
    return _maxId + 1;
  }

  @override
  void add(T model) {
    _models.add(model);
    if (model.id > _maxId) _maxId = model.id;
    _save();
    print('add $model');
  }

  @override
  void remove(int id) {
    _onRemove.forEach((onRemove) => onRemove(id));

    _models.removeWhere((model) => model.id == id);
    if (id == _maxId) _findMaxId();
    _save();
    print('remove $id');
  }

  void modify(T model) {
    if (has(model.id)) {
      final index = _models.indexWhere((m) => m.id == model.id);
      _models.replaceRange(index, index + 1, [model]);
      _save();
      print('modify $model');
      return;
    }
    add(model);
  }
}
