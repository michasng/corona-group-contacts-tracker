import 'package:Group_Presence/models/model.dart';
import 'package:Group_Presence/services/local-storage/local-storage-model.service.dart';

class LocalStorageModelContext {
  Map<Type, LocalStorageModelService> map;

  LocalStorageModelContext() {
    map = Map();
  }

  void registerService<T extends Model>(LocalStorageModelService<T> service) {
    map[T] = service;
    print('registered service for type $T');
    map.values.forEach((service) {
      service.onContextChanged();
    });
  }

  LocalStorageModelService<T> serviceOf<T extends Model>() {
    assert(T != dynamic);
    return map[T];
  }
}
