import 'package:Group_Presence/models/group.model.dart';
import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/widgets/model-checklist.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GroupsChecklist extends StatefulWidget {
  final List<GroupModel> groups;
  final List<int> selectedPersons;
  final SelectionChanged onChanged;

  GroupsChecklist({
    Key key,
    @required this.groups,
    @required this.selectedPersons,
    @required this.onChanged,
  }) : super(key: key);

  @override
  _GroupsChecklistState createState() => _GroupsChecklistState();
}

class _GroupsChecklistState extends State<GroupsChecklist> {
  List<bool> panelsExpanded;

  @override
  void initState() {
    super.initState();

    panelsExpanded = List();
    panelsExpanded =
        List.generate(widget.groups.length, (index) => false, growable: false);
  }

  @override
  Widget build(BuildContext context) {
    final personService = context.watch<PersonService>();

    return ExpansionPanelList(
      expansionCallback: (panelIndex, isExpanded) =>
          setState(() => panelsExpanded[panelIndex] = !isExpanded),
      children: widget.groups
          .asMap()
          .map((index, group) => MapEntry(
                index,
                ExpansionPanel(
                  isExpanded: panelsExpanded[index],
                  headerBuilder: (context, isExpanded) {
                    final isSelected =
                        (personId) => widget.selectedPersons.contains(personId);
                    final groupValue = group.members.any(isSelected)
                        ? (group.members.every(isSelected) ? true : null)
                        : false;
                    return ListTile(
                      title: Text(group.name),
                      subtitle: isExpanded
                          ? Text(group.members.length == 1
                              ? '${group.members.length} Mitglied'
                              : '${group.members.length} Mitglieder')
                          : null,
                      leading: Checkbox(
                        // ToDo: remove checkbox when group is empty
                        tristate: true,
                        value: groupValue,
                        onChanged: (anyValue) {
                          // when group is selected -> disable all
                          // when group is unselected or partially selected -> enable all
                          group.members.forEach((personId) {
                            // NOTE: "== true" is required, groupValue can be null
                            if ((groupValue == true) == isSelected(personId))
                              widget.onChanged(personId, (groupValue != true));
                          });
                        },
                      ),
                    );
                  },
                  canTapOnHeader: true,
                  body: ModelChecklist<PersonModel>(
                    models: group.members
                        .map((id) => personService.getOne(id))
                        .toList(),
                    selectedIds: widget.selectedPersons,
                    titleBuilder: (model) =>
                        '${model.lastName}, ${model.firstName}',
                    onChanged: widget.onChanged,
                  ),
                ),
              ))
          .values
          .toList(),
    );
  }
}
