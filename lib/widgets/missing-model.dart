import 'package:flutter/material.dart';

class MissingModel extends StatelessWidget {
  final String title;
  final int tabs;
  final int arrowToTab;
  final IconData tabIcon;

  const MissingModel({
    Key key,
    @required this.title,
    @required this.tabIcon,
    @required this.tabs,
    @required this.arrowToTab,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            title,
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Icon(tabIcon),
          ),
        ],
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: List.generate(
          tabs,
          (index) => Icon(
            index == arrowToTab ? Icons.keyboard_arrow_down : null,
            color: Theme.of(context).accentColor,
            size: 64,
          ),
        ),
      ),
    );
  }
}
