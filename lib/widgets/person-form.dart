import 'package:Group_Presence/models/entry.model.dart';
import 'package:Group_Presence/models/group.model.dart';
import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/entry.service.dart';
import 'package:Group_Presence/services/group.service.dart';
import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/widgets/date-time-selector.dart';
import 'package:Group_Presence/widgets/model-checklist.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonForm extends StatefulWidget {
  final int id;
  final Function() onSave;

  PersonForm({Key key, this.id, this.onSave}) : super(key: key);

  @override
  _PersonFormState createState() => _PersonFormState();
}

class _PersonFormState extends State<PersonForm> {
  final _formKey = GlobalKey<FormState>();
  var _autoValidateMode = AutovalidateMode.disabled;
  PersonModel _person;
  List<int> _groups;
  bool _shouldAddEntry;
  EntryModel _entry;

  @override
  void initState() {
    super.initState();

    var personService = context.read<PersonService>();
    _groups = [];
    if (widget.id != null) {
      _person = personService.getOne(widget.id).clone();
      final groupService = context.read<GroupService>();
      for (var group in groupService.getAll()) {
        if (group.members.contains(_person.id)) _groups.add(group.id);
      }
    } else {
      _person = PersonModel(
        id: personService.getNewId(),
        firstName: '',
        lastName: '',
        phoneNumber: '',
        emailAddress: '',
        registerTimestamp: 0,
      );
    }

    _shouldAddEntry = false;
    final now = DateTime.now();
    // current time rounded down to minute accuracy
    final _from = DateTime(now.year, now.month, now.day, now.hour, now.minute);
    // set default duration
    final _to = _from.add(Duration(hours: 1));
    var entryService = context.read<EntryService>();
    _entry = EntryModel(
      id: entryService.getNewId(),
      persons: [_person.id],
      fromTimestamp: _from.millisecondsSinceEpoch,
      toTimestamp: _to.millisecondsSinceEpoch,
    );
  }

  void _submit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _person.registerTimestamp = DateTime.now().millisecondsSinceEpoch;
      var personService = context.read<PersonService>();
      personService.modify(_person);
      final groupService = context.read<GroupService>();
      for (var group in groupService.getAll()) {
        if (group.members.contains(_person.id) && !_groups.contains(group.id)) {
          group.members.remove(_person.id);
          groupService.modify(group);
        } else if (!group.members.contains(_person.id) &&
            _groups.contains(group.id)) {
          group.members.add(_person.id);
          groupService.modify(group);
        }
      }
      if (_shouldAddEntry) {
        var entryService = context.read<EntryService>();
        entryService.add(_entry);
      }
      widget.onSave();
    } else {
      setState(() {
        _autoValidateMode = AutovalidateMode.always;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final groupService = context.watch<GroupService>();
    final groupsExist = groupService.hasAny();
    return Scaffold(
      body: Form(
        key: _formKey,
        autovalidateMode: _autoValidateMode,
        child: ListView(
          children: [
            ..._buildFormFields(context),
            if (groupsExist)
              ListTile(
                title: Text('Gruppen'),
              ),
            if (groupsExist)
              ModelChecklist<GroupModel>(
                models: groupService.getAll(),
                selectedIds: _groups,
                onChanged: (id, value) {
                  if (value) {
                    if (!_groups.contains(id)) // just to be sure
                      setState(() => _groups.add(id));
                  } else {
                    setState(() => _groups.remove(id));
                  }
                },
                titleBuilder: (model) => model.name,
              ),
            SwitchListTile(
              title: Text('Anwesenheit anmelden'),
              value: _shouldAddEntry,
              onChanged: (newValue) =>
                  setState(() => _shouldAddEntry = newValue),
            ),
            if (_shouldAddEntry)
              DateTimeSelector(
                initialFrom: _entry.fromTimestamp,
                initialTo: _entry.toTimestamp,
                dateTimesChanged: (from, to) {
                  _entry.fromTimestamp = from.millisecondsSinceEpoch;
                  _entry.toTimestamp = to.millisecondsSinceEpoch;
                },
              ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.save),
        onPressed: _submit,
      ),
    );
  }

  List<Widget> _buildFormFields(BuildContext context) {
    return [
      TextFormField(
        initialValue: _person.lastName,
        decoration: const InputDecoration(
          hintText: 'Name',
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        validator: (value) {
          if (value.isEmpty) return 'Bitte einen Namen eingeben';
          return null;
        },
        onSaved: (value) => _person.lastName = value,
      ),
      TextFormField(
        initialValue: _person.firstName,
        decoration: const InputDecoration(
          hintText: 'Vorname',
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        validator: (value) {
          if (value.isEmpty) return 'Bitte einen Vornamen eingeben';
          return null;
        },
        onSaved: (value) => _person.firstName = value,
      ),
      TextFormField(
        initialValue: _person.phoneNumber,
        decoration: const InputDecoration(
          hintText: 'Telefonnummer',
        ),
        keyboardType: TextInputType.phone,
        textInputAction: TextInputAction.next,
        validator: _validatePhoneNumber,
        onSaved: (value) => _person.phoneNumber = value,
      ),
      TextFormField(
        initialValue: _person.emailAddress,
        decoration: const InputDecoration(
          hintText: 'E-Mail-Adresse',
        ),
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.done,
        onFieldSubmitted: (_value) => _submit(),
        validator: _validateEmail,
        onSaved: (value) => _person.emailAddress = value,
      ),
    ];
  }

  String _validatePhoneNumber(String value) {
    Pattern pattern = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Bitte eine gültige Telefonnummer eingeben';
    else
      return null;
  }

  String _validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Bitte eine gültige E-Mail-Adresse eingeben';
    else
      return null;
  }
}
