import 'package:Group_Presence/models/entry.model.dart';
import 'package:Group_Presence/services/entry.service.dart';
import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/services/time.service.dart';
import 'package:Group_Presence/widgets/missing-model.dart';
import 'package:Group_Presence/widgets/model-list.dart';
import 'package:Group_Presence/widgets/pages/entry-page.dart';
import 'package:excel/excel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:universal_html/html.dart' as html;

class EntriesTab extends StatelessWidget {
  const EntriesTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final entryService = context.watch<EntryService>();
    if (!entryService.hasAny())
      return MissingModel(
        title: 'Es wurden noch keine Anwesenheiten erfasst.',
        tabIcon: Icons.access_time,
        tabs: 3,
        arrowToTab: 1,
      );
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: ModelList<EntryModel>(
                service: entryService,
                singular: 'Eintrag',
                plural: 'Einträge',
                titleBuilder: (model) {
                  final from =
                      DateTime.fromMillisecondsSinceEpoch(model.fromTimestamp);
                  final to =
                      DateTime.fromMillisecondsSinceEpoch(model.toTimestamp);
                  // check if from and to are the same day
                  return (from.year == to.year &&
                          from.month == to.month &&
                          from.day == to.day)
                      ? '${TimeService.toTimeString(from)} bis ${TimeService.toTimeString(to)}'
                      : '${TimeService.toTimeString(from)} bis ${TimeService.toDateTimeString(to)}';
                },
                subtitleBuilder: (model) {
                  // for just one person, show full name
                  if (model.persons.length == 1) {
                    final personService = context.read<PersonService>();
                    final person = personService.getOne(model.persons[0]);
                    return '${person.lastName}, ${person.firstName}';
                  } else {
                    // otherwise show amount of people
                    return '${model.persons.length} Personen';
                  }
                },
                editPageRouteName: EntryPage.route,
                hideAddButton: true,
                groupByCompare: (m1, m2) =>
                    TimeService.compare(m1.fromTimestamp, m2.fromTimestamp),
                groupTitleBuilder: (model) =>
                    TimeService.timestampToDateString(model.fromTimestamp),
              ),
            ),
          ),
          ButtonBar(
            children: [
              OutlineButton(
                child: Text('Excel-Datei exportieren'),
                onPressed: () => _export(context),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _export(BuildContext context) {
    var excel = Excel.createExcel();
    Sheet sheet = excel['Sheet1'];
    Data _getCell(int col, int row) {
      return sheet
          .cell(CellIndex.indexByColumnRow(columnIndex: col, rowIndex: row));
    }

    CellStyle cellStyle = CellStyle(
      backgroundColorHex: '#D1F1DA',
      underline: Underline.Single,
      bold: true,
    );
    void _setHeaderValue(int col, String value) {
      var cell = _getCell(col, 0);
      cell.value = value;
      cell.cellStyle = cellStyle;
    }

    var col = 0;
    _setHeaderValue(col++, 'Name');
    _setHeaderValue(col++, 'Vorname');
    _setHeaderValue(col++, 'Telefonnummer');
    _setHeaderValue(col++, 'E-Mail Adresse');
    _setHeaderValue(col++, 'Von');
    _setHeaderValue(col++, 'Bis');

    final entryService = context.read<EntryService>();
    final personService = context.read<PersonService>();
    var row = 1;
    for (var entry in entryService.getAll()) {
      for (var personId in entry.persons) {
        final person = personService.getOne(personId);
        col = 0;
        _getCell(col++, row).value = person.lastName;
        _getCell(col++, row).value = person.firstName;
        _getCell(col++, row).value = person.phoneNumber;
        _getCell(col++, row).value = person.emailAddress;
        _getCell(col++, row).value =
            TimeService.timestampToDateTimeString(entry.fromTimestamp);
        _getCell(col++, row).value =
            TimeService.timestampToDateTimeString(entry.toTimestamp);
        row++;
      }
    }

    _saveFile(excel);
  }

  void _saveFile(Excel excel) async {
    // prepare
    final bytes = await excel.encode();
    // final bytes = utf8.encode(text);
    final blob = html.Blob([bytes]);
    final url = html.Url.createObjectUrlFromBlob(blob);
    final anchor = html.document.createElement('a') as html.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..download = 'export.xlsx';
    html.document.body.children.add(anchor);

    // download
    anchor.click();

    // cleanup
    html.document.body.children.remove(anchor);
    html.Url.revokeObjectUrl(url);
  }
}
