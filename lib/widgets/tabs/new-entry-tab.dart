import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/widgets/entry-form.dart';
import 'package:Group_Presence/widgets/missing-model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NewEntryTab extends StatelessWidget {
  final Function() onSave;
  NewEntryTab({Key key, this.onSave}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final entryService = context.watch<PersonService>();
    if (!entryService.hasAny())
      return MissingModel(
        title: 'Es wurden noch keine Personen angemeldet.',
        tabIcon: Icons.group,
        tabs: 3,
        arrowToTab: 0,
      );
    return Padding(
      padding: EdgeInsets.all(16),
      child: EntryForm(onSave: onSave),
    );
  }
}
