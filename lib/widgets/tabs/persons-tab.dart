import 'package:Group_Presence/models/group.model.dart';
import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/group.service.dart';
import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/widgets/model-list.dart';
import 'package:Group_Presence/widgets/pages/group-page.dart';
import 'package:Group_Presence/widgets/pages/person-page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonsTab extends StatelessWidget {
  const PersonsTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: [
          ListTile(
            title: Text('Personen'),
          ),
          ModelList<PersonModel>(
            service: context.watch<PersonService>(),
            singular: 'Person',
            plural: 'Personen',
            titleBuilder: (model) => '${model.lastName}, ${model.firstName}',
            subtitleBuilder: (model) =>
                '${model.phoneNumber}, ${model.emailAddress}',
            editPageRouteName: PersonPage.route,
          ),
          ListTile(
            title: Text('Gruppen'),
          ),
          ModelList<GroupModel>(
            service: context.watch<GroupService>(),
            singular: 'Gruppe',
            plural: 'Gruppen',
            titleBuilder: (model) => model.name,
            subtitleBuilder: (model) {
              // for just one person, show full name
              if (model.members.length == 1) {
                final personService = context.read<PersonService>();
                final person = personService.getOne(model.members[0]);
                return '${person.lastName}, ${person.firstName}';
              } else {
                // otherwise show amount of members
                return '${model.members.length} Mitglieder';
              }
            },
            editPageRouteName: GroupPage.route,
          ),
        ],
      ),
    );
  }
}
