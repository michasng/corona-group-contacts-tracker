import 'package:Group_Presence/models/model.dart';
import 'package:flutter/material.dart';

typedef SelectionChanged = void Function(int id, bool value);

class ModelChecklist<T extends Model> extends StatelessWidget {
  final List<Model> models;
  final List<int> selectedIds;
  final SelectionChanged onChanged;

  final String Function(T model) titleBuilder;

  ModelChecklist({
    Key key,
    @required this.models,
    @required this.selectedIds,
    @required this.onChanged,
    @required this.titleBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: models
          .map((model) => CheckboxListTile(
                title: Text(titleBuilder(model)),
                value: selectedIds.contains(model.id),
                controlAffinity: ListTileControlAffinity.leading,
                onChanged: (bool value) {
                  onChanged(model.id, value);
                },
              ))
          .toList(),
    );
  }
}
