import 'package:Group_Presence/models/entry.model.dart';
import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/entry.service.dart';
import 'package:Group_Presence/services/group.service.dart';
import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/services/time.service.dart';
import 'package:Group_Presence/widgets/date-time-selector.dart';
import 'package:Group_Presence/widgets/groups-checklist.dart';
import 'package:Group_Presence/widgets/model-checklist.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EntryForm extends StatefulWidget {
  final int id;
  final Function() onSave;

  EntryForm({Key key, this.id, this.onSave}) : super(key: key);

  @override
  _EntryFormState createState() => _EntryFormState();
}

class _EntryFormState extends State<EntryForm> {
  EntryModel _entry;

  @override
  void initState() {
    super.initState();

    var entryService = context.read<EntryService>();
    if (widget.id != null) {
      _entry = entryService.getOne(widget.id).clone();
    } else {
      final now = DateTime.now();
      // current time rounded down to minute accuracy
      final _from =
          DateTime(now.year, now.month, now.day, now.hour, now.minute);
      // set default duration
      final _to = _from.add(Duration(hours: 1));
      _entry = EntryModel(
        id: entryService.getNewId(),
        persons: [],
        fromTimestamp: _from.millisecondsSinceEpoch,
        toTimestamp: _to.millisecondsSinceEpoch,
      );
    }
  }

  void _accept() {
    var entryService = context.read<EntryService>();
    entryService.modify(_entry);
    widget.onSave();
  }

  @override
  Widget build(BuildContext context) {
    final groupService = context.watch<GroupService>();
    final personService = context.watch<PersonService>();
    return Scaffold(
      body: ListView(
        children: [
          DateTimeSelector(
            initialFrom: _entry.fromTimestamp,
            initialTo: _entry.toTimestamp,
            dateTimesChanged: (from, to) {
              _entry.fromTimestamp = from.millisecondsSinceEpoch;
              _entry.toTimestamp = to.millisecondsSinceEpoch;
              print(
                  'date-times changed: ${TimeService.toDateTimeString(from)} to ${TimeService.toDateTimeString(to)}');
            },
          ),
          ListTile(
            title: Text('Gruppen anmelden'),
            subtitle: groupService.hasAny()
                ? null
                : Text(
                    'Erstelle zunächst eine Gruppe, um die Anwesenheit mehrerer Personen auf einmal anzumelden'),
          ),
          GroupsChecklist(
            groups: groupService.getAll(),
            selectedPersons: _entry.persons,
            onChanged: _onPersonSelectionChanged,
          ),
          ListTile(
            title: Text('Personen einzeln anmelden'),
          ),
          ModelChecklist<PersonModel>(
            models: personService.getAll(),
            selectedIds: _entry.persons,
            titleBuilder: (model) => '${model.lastName}, ${model.firstName}',
            onChanged: _onPersonSelectionChanged,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _entry.persons.isEmpty ? null : _accept,
        child: Icon(Icons.save),
        backgroundColor:
            _entry.persons.isEmpty ? Theme.of(context).disabledColor : null,
      ),
    );
  }

  void _onPersonSelectionChanged(id, value) {
    if (value) {
      if (!_entry.persons.contains(id)) // just to be sure
        setState(() => _entry.persons.add(id));
    } else {
      setState(() => _entry.persons.remove(id));
    }
  }
}
