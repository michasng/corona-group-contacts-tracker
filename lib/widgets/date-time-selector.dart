import 'package:Group_Presence/services/time.service.dart';
import 'package:flutter/material.dart';

class DateTimeSelector extends StatefulWidget {
  final int initialFrom, initialTo;
  final Function(DateTime from, DateTime to) dateTimesChanged;

  DateTimeSelector({
    Key key,
    @required this.initialFrom,
    @required this.initialTo,
    @required this.dateTimesChanged,
  }) : super(key: key);

  @override
  _DateTimeSelectorState createState() => _DateTimeSelectorState();
}

class _DateTimeSelectorState extends State<DateTimeSelector> {
  DateTime _from, _to;
  @override
  void initState() {
    super.initState();

    _from = DateTime.fromMillisecondsSinceEpoch(widget.initialFrom);
    _to = DateTime.fromMillisecondsSinceEpoch(widget.initialTo);
  }

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(
      color: Theme.of(context).accentColor,
    );

    return Column(
      children: [
        ListTile(
          leading: Text(''),
          title: GestureDetector(
            onTap: () async {
              await _setFrom(_pickDate(_from));
            },
            child: Text(
              TimeService.toDateString(_from),
              style: textStyle,
            ),
          ),
          trailing: GestureDetector(
            onTap: () async {
              await _setFrom(_pickTime(_from));
            },
            child: Text(
              TimeService.toTimeString(_from),
              style: textStyle,
            ),
          ),
        ),
        ListTile(
          leading: Text(''),
          title: GestureDetector(
            onTap: () async {
              await _setTo(_pickDate(_to));
            },
            child: Text(
              TimeService.toDateString(_to),
              style: textStyle,
            ),
          ),
          trailing: GestureDetector(
            onTap: () async {
              await _setTo(_pickTime(_to));
            },
            child: Text(
              TimeService.toTimeString(_to),
              style: textStyle,
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _setFrom(Future<DateTime> dateTimeFuture) async {
    final dateTime = await dateTimeFuture;
    if (dateTime == null) return;
    setState(() {
      final difference = dateTime.difference(_from);
      _from = dateTime;
      _to = _to.add(difference);
    });
    widget.dateTimesChanged(_from, _to);
  }

  Future<void> _setTo(Future<DateTime> dateTimeFuture) async {
    final dateTime = await dateTimeFuture;
    if (dateTime == null) return;
    setState(() {
      _to = dateTime;
      if (_to.isBefore(_from)) _from = _to;
    });
    widget.dateTimesChanged(_from, _to);
  }

  Future<DateTime> _pickDate(DateTime original) async {
    var datetime = await showDatePicker(
      context: context,
      initialDate: original,
      firstDate: original.subtract(Duration(days: 356)),
      lastDate: original.add(Duration(days: 356)),
    );
    if (datetime == null) return null;
    print('Date picked $datetime');
    return DateTime(datetime.year, datetime.month, datetime.day, original.hour,
        original.minute);
  }

  Future<DateTime> _pickTime(DateTime original) async {
    final timeOfDay = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(original),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );
    if (timeOfDay == null) return null;
    print('Time picked $timeOfDay');
    return DateTime(original.year, original.month, original.day, timeOfDay.hour,
        timeOfDay.minute);
  }
}
