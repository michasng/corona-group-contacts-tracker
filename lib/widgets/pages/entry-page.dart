import 'package:Group_Presence/widgets/entry-form.dart';
import 'package:flutter/material.dart';

class EntryPage extends StatelessWidget {
  static const String route = '/entry';

  final int id;
  EntryPage({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Eintrag bearbeiten'),
      ),
      body: EntryForm(
        id: id,
        onSave: () => Navigator.of(context).pop(),
      ),
    );
  }
}
