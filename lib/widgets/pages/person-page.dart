import 'package:Group_Presence/widgets/person-form.dart';
import 'package:flutter/material.dart';

class PersonPage extends StatelessWidget {
  static const String route = '/person';

  final int id;

  PersonPage({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Person anmelden'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: PersonForm(
          id: id,
          onSave: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }
}
