import 'package:Group_Presence/widgets/group-form.dart';
import 'package:flutter/material.dart';

class GroupPage extends StatelessWidget {
  static const String route = '/group';

  final int id;
  GroupPage({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gruppe bearbeiten'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: GroupForm(
          id: id,
          onSave: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }
}
