import 'package:Group_Presence/widgets/pages/settings-page.dart';
import 'package:Group_Presence/widgets/tabs/entries-tab.dart';
import 'package:Group_Presence/widgets/tabs/new-entry-tab.dart';
import 'package:Group_Presence/widgets/tabs/persons-tab.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static const String route = '/home';

  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex;
  List<Widget> _widgetOptions;

  @override
  void initState() {
    super.initState();
    _widgetOptions = <Widget>[
      PersonsTab(),
      NewEntryTab(onSave: () => _onItemTapped(2)), // on save: go to entries tab
      EntriesTab(),
    ];
    _selectedIndex = 1;
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Anwesenheits-Erfassung'),
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: _pushSettingsPage,
          ),
        ],
      ),
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.group),
            label: 'Personen',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.schedule),
            label: 'Neuer Eintrag',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Einträge',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }

  void _pushSettingsPage() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) => SettingsPage(),
      ),
    );
  }
}
