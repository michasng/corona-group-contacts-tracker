import 'package:Group_Presence/main.dart';
import 'package:Group_Presence/models/model.dart';
import 'package:Group_Presence/services/model.service.dart';
import 'package:flutter/material.dart';

class ModelList<T extends Model> extends StatelessWidget {
  final ModelService<T> service;
  final String singular, plural;
  final String Function(T model) titleBuilder;
  final String Function(T model) subtitleBuilder;
  final String editPageRouteName;
  final bool hideAddButton;
  final int Function(T m1, T m2) groupByCompare;
  final String Function(T model) groupTitleBuilder;

  ModelList({
    Key key,
    @required this.service,
    @required this.singular,
    @required this.plural,
    @required this.titleBuilder,
    @required this.subtitleBuilder,
    @required this.editPageRouteName,
    this.hideAddButton = false,
    this.groupByCompare,
    this.groupTitleBuilder,
  }) : super(key: key) {
    // either both must be null, or neither
    assert((groupByCompare == null) == (groupTitleBuilder == null));
  }

  @override
  Widget build(BuildContext context) {
    final models = service.getAll();
    final doGrouping = (this.groupByCompare != null);
    if (doGrouping) models.sort(groupByCompare);

    var lastGroupTitle;
    return Column(
      children: [
        ...models.map<Widget>(
          (model) {
            if (doGrouping) {
              final groupTitle = groupTitleBuilder(model);
              if (groupTitle != lastGroupTitle) {
                lastGroupTitle = groupTitle;
                return Column(
                  children: [
                    ListTile(
                      title: Text(groupTitle),
                    ),
                    _buildModelCard(context, model),
                  ],
                );
              }
              return _buildModelCard(context, model);
            }
            return _buildModelCard(context, model);
          },
        ),
        if (!hideAddButton)
          IconButton(
            icon: Icon(Icons.add),
            color: Theme.of(context).accentColor,
            onPressed: () => _pushEditPage(context: context, model: null),
          ),
      ],
    );
  }

  Widget _buildModelCard(BuildContext context, T model) {
    return Card(
      child: ListTile(
        title: Text(titleBuilder(model)),
        subtitle: Text(subtitleBuilder(model)),
        trailing: IconButton(
          icon: Icon(Icons.delete),
          color: Theme.of(context).accentColor,
          onPressed: () =>
              _showRemoveModelDialog(context: context, model: model),
        ),
        onTap: () => _pushEditPage(context: context, model: model),
      ),
    );
  }

  void _pushEditPage({@required BuildContext context, @required T model}) {
    Navigator.of(context).pushNamed(editPageRouteName,
        arguments: model == null ? null : IdArguments(model.id));
  }

  void _showRemoveModelDialog(
      {@required BuildContext context, @required T model}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('$singular löschen?'),
          content:
              Text('Gelöschte $plural können nicht wiederhergestellt werden.'),
          actions: <Widget>[
            FlatButton(
              child: Text('Abbrechen'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            FlatButton(
              child: Text('Löschen'),
              onPressed: () {
                service.remove(model.id);
                Navigator.of(context).pop();
              },
              textColor: Theme.of(context).accentColor,
            ),
          ],
        );
      },
    );
  }
}
