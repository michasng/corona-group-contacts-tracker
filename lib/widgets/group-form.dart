import 'package:Group_Presence/models/group.model.dart';
import 'package:Group_Presence/models/person.model.dart';
import 'package:Group_Presence/services/group.service.dart';
import 'package:Group_Presence/services/person.service.dart';
import 'package:Group_Presence/widgets/model-checklist.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GroupForm extends StatefulWidget {
  final int id;
  final Function() onSave;

  GroupForm({Key key, this.id, this.onSave}) : super(key: key);

  @override
  _GroupFormState createState() => _GroupFormState();
}

class _GroupFormState extends State<GroupForm> {
  final _formKey = GlobalKey<FormState>();
  var _autoValidateMode = AutovalidateMode.disabled;
  GroupModel _group;

  @override
  void initState() {
    super.initState();

    var groupService = context.read<GroupService>();
    if (widget.id != null) {
      _group = groupService.getOne(widget.id).clone();
    } else {
      _group = GroupModel(
        id: groupService.getNewId(),
        name: '',
        members: [],
      );
    }
  }

  void _accept() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      var groupService = context.read<GroupService>();
      groupService.modify(_group);
      widget.onSave();
    } else {
      setState(() {
        _autoValidateMode = AutovalidateMode.always;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final personService = context.watch<PersonService>();
    return Scaffold(
      body: Form(
        key: _formKey,
        autovalidateMode: _autoValidateMode,
        child: ListView(
          children: [
            TextFormField(
              initialValue: _group.name,
              decoration: const InputDecoration(
                hintText: 'Name',
              ),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              validator: (value) {
                if (value.isEmpty) return 'Bitte einen Namen eingeben';
                return null;
              },
              onSaved: (value) => _group.name = value,
            ),
            ListTile(
              title: Text('Mitglieder'),
              subtitle: personService.hasAny()
                  ? null
                  : Text('Es wurden noch keine Personen angemeldet.'),
            ),
            ModelChecklist<PersonModel>(
              models: personService.getAll(),
              selectedIds: _group.members,
              titleBuilder: (model) => '${model.lastName}, ${model.firstName}',
              onChanged: (id, value) {
                if (value) {
                  if (!_group.members.contains(id)) // just to be sure
                    setState(() => _group.members.add(id));
                } else {
                  setState(() => _group.members.remove(id));
                }
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _accept,
        child: Icon(Icons.save),
      ),
    );
  }
}
