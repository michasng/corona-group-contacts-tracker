# Corona Group Presence

A Flutter App to track contacts between groups of people. This App can be used to track whenever individuals or groups of people meet in one place.

## Description

Users must be registered when they're using the app for the first time. People can also be grouped to act as a collection. New time entries can be created that track the presence of individual people of groups. All entries are listed in a neat overview and can be exported to Microsoft Excel.

## Getting Started

Run the flutter project using `flutter run`. It might be advantageous to run multiple instances, to see how firebase automatically updates all local databases.

## Authors

Micha Sengotta
micha.sengotta@gmail.com

